# Project Library
## _TP Final - Desarrollo de Aplicaciones Web_


### Proyecto Administración de una biblioteca escolar. 

## Integrantes
- Hendel, Florencia.
- Riverol, Daniel.
## Docente
- Pablo Ingino

## Características

- Alta, baja y modificación de libros.
- Visualización de catálogo de libros.
- Préstamo de libros.
- Visualización listado de estudiantes.

## Tecnologías

Desarrollado con las siguientes tecnologías: 

- [PHP] - Backend
- [MySQL] - Base de datos
- [SB Admin](https://startbootstrap.com/theme/sb-admin) - Plantilla Panel de Administración Bootstrap 5.

